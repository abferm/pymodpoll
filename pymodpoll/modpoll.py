import commands
import collections

protocols = {
    'ascii': '-m ascii',
    'rtu': '-m rtu',
    'tcp': '-m tcp',
    'enc': '-m enc'}
data_types = {
    'int': '',
    'float': ':float',
    'mod': ':mod',
    'long': ':int',
    'hex': ':hex',
    'raw': ':hex',
    int: '',
    float: ':float',
    long: ':int'}
type_casts = {
    'int': int,
    'float': float,
    'mod': long,
    'long': long,
    'hex': str,
    'raw': lambda hex: bytearray.fromhex(hex.split('x')[-1])}
parity_types = {'none': 'none', None: 'none', 'even': 'even', 'odd': 'odd'}


def modpoll(protocol, register, port_or_host, slave_address=1, table=4, count=0, data_type='int', big_endian=False, enron=False,
            indexing=1, timeout=1.0, tcp_port=502, baudrate=19200, data_bits=8, stop_bits=1, parity='even', rts_ms=None, write_values=None):
    protocol = protocols[protocol]
    table_string = '-t %d%s' % (table, data_types[data_type])

    cast = type_casts.get(data_type, data_type)

    tcp_opts = '-p %d' % tcp_port
    serial_opts = '-b %d -d %d -s %d -p %s' % (
        baudrate, data_bits, stop_bits, parity_types[parity])

    if rts_ms:
        serial_opts += ' -4 %d' % rts_ms

    universal_opts = '%s %s -a %d -r %d -o %f' % (
        protocol, table_string, slave_address, register, timeout)

    if count and not write_values:
        universal_opts += ' -c %d' % count

    if big_endian:
        universal_opts += ' -i -f'

    if enron:
        universal_opts += ' -e'

    if indexing == 0:
        universal_opts += ' -0'

    if protocol in [protocols['tcp'], protocols['enc']]:
        options = ' '.join([universal_opts, tcp_opts])
    else:
        options = ' '.join([universal_opts, serial_opts])

    if write_values != None:
        write_str = '--'

        if data_type == 'raw':
            raise TypeError('Can not write data_type \'raw\'.')

        if isinstance(write_values, collections.Iterable):
            for i in write_values:
                write_str += ' %s' % str(cast(i))
        else:
            write_str += ' %s' % str(cast(write_values))

        command_str = ' '.join(['modpoll', options, port_or_host, write_str])

    else:
        command_str = ' '.join(['modpoll', options, '-1', port_or_host])

    shell_result = commands.getstatusoutput(command_str)

    if (shell_result[0] == 0):
        if write_values != None:
            return True

        try:
            if count > 1:
                results = shell_result[1].splitlines()[-count:]
                for i in range(count):
                    results[i] = cast(results[i].split(":")[-1].strip())
                return results
            return cast(shell_result[1].splitlines()[-1].split(":")[-1].strip())
        except:
            raise ValueError(shell_result)
    else:
        raise ValueError(shell_result)
